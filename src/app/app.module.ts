import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";

import { AppComponent } from "./app.component";
import { MatchesComponent } from "./pages/matches/matches.component";
import { ClientsComponent } from "./pages/clients/clients.component";
import { MenuComponent } from "./components/menu/menu.component";
import { HomeComponent } from "./pages/home/home.component";
import { MDBBootstrapModule } from "angular-bootstrap-md";
import { FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  MatTabsModule,
  MatTabGroup,
  MatIcon,
  MatIconModule
} from "@angular/material";
import { SocketsComponent } from "./components/tables/sockets/sockets.component";
import { DriversComponent } from "./components/tables/drivers/drivers.component";
import { ClientsTableComponent } from "./components/tables/clients-table/clients-table.component";
import { SocketioService } from './services/socketio.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MatchesComponent,
    ClientsComponent,
    MenuComponent,
    SocketsComponent,
    DriversComponent,
    ClientsTableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatIconModule
  ],
  providers: [SocketioService],
  bootstrap: [AppComponent]
})
export class AppModule {}
