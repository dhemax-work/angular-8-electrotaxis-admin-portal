import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  routes = [
    {
      name: 'INICIO',
      path: '/home'
    },
    {
      name: 'CONEXIONES',
      path: '/conns'
    },
    {
      name: 'MATCHES',
      path: '/matches'
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}
