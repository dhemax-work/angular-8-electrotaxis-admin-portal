import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { SocketioService } from 'src/app/services/socketio.service';
import { MdbTablePaginationComponent, MdbTableDirective, ModalDirective } from 'angular-bootstrap-md';

@Component({
  selector: 'app-drivers',
  templateUrl: './drivers.component.html',
  styleUrls: ['./drivers.component.scss']
})
export class DriversComponent implements OnInit {
  constructor(
    private cdRef: ChangeDetectorRef,
    private SocketService: SocketioService
  ) {}
  @ViewChild(MdbTablePaginationComponent, { static: true })
  mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  @ViewChild("viewProfileModal", { static: false })
  ShowModalOnClick: ModalDirective;

  clients: any = [];
  elements: any = [];
  previous: any = [];
  ClientInfo: any = [];

  headElements = ["VehicleID", "SocketID", "rfID", "ACTIONS"];

  public showModal(clientID) {
    this.ClientInfo = [];
    this.ClientInfo.push(this.getVehicleData(clientID));
    if (this.ClientInfo !== undefined) {
      this.ShowModalOnClick.show();
    } else {
      console.log("User information cant be given");
    }
  }

  public getVehicleData(vehicleid) {
    let userinfo;
    this.clients.forEach((element, i) => {
      if (element.VehicleID === vehicleid) {
        userinfo = {
          avatar: "https://placekitten.com/200/300",
          name: "V Alex " + i,
          xposition: element.Location.XPosition,
          zposition: element.Location.ZPosition,
          vehicleid: element.VehicleID,
          socketid: element.SocketID,
          status: element.Status,
          rfid: element.rfID
        };
      }
    });

    return userinfo;
  }

  ngOnInit() {
    this.loadTableComponents();
  }

  loadTableComponents() {
    this.SocketService.getVehicles().subscribe(data => {
      data.forEach(element => {
        this.clients.push(element);
      });

      this.clients.forEach((element, i) => {
        
        
        this.elements.push({
          vehicleid: element.VehicleID,
          socketid: element.SocketID,
          rfid: element.rfID
        });

        this.mdbTable.setDataSource(this.elements);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
      });
    });
  }

  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(5);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }
}
