import { SocketioService } from "../../../services/socketio.service";
import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ChangeDetectorRef
} from "@angular/core";
import {
  MdbTablePaginationComponent,
  MdbTableDirective,
  ModalDirective
} from "angular-bootstrap-md";

@Component({
  selector: "app-clients-table",
  templateUrl: "./clients-table.component.html",
  styleUrls: ["./clients-table.component.scss"]
})
export class ClientsTableComponent implements OnInit, AfterViewInit {
  constructor(
    private cdRef: ChangeDetectorRef,
    private SocketService: SocketioService
  ) {}
  @ViewChild(MdbTablePaginationComponent, { static: true })
  mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  @ViewChild("viewProfileModal", { static: false })
  ShowModalOnClick: ModalDirective;

  clients: any = [];
  elements: any = [];
  previous: any = [];
  ClientInfo: any = [];

  headElements = ["ClientID", "SocketID", "IP", "ACTIONS"];

  public showModal(clientID) {
    this.ClientInfo = [];
    this.ClientInfo.push(this.getClientData(clientID));
    if (this.ClientInfo !== undefined) {
      console.log(this.ClientInfo);

      this.ShowModalOnClick.show();
    } else {
      console.log("User information cant be given");
    }
  }

  public getClientData(clientid) {
    let userinfo;
    this.clients.forEach((element, i) => {
      if (element.ClientID === clientid) {
        userinfo = {
          avatar: "https://placekitten.com/200/300",
          name: "Alex " + i,
          xposition: element.ClientPosition.XPosition,
          zposition: element.ClientPosition.ZPosition,
          clientid: element.ClientID,
          socketid: element.SocketID,
          ipaddress: element.ClientIP
        };
      }
    });

    return userinfo;
  }

  ngOnInit() {
    this.loadTableComponents();
  }

  loadTableComponents() {
    this.SocketService.getClients().subscribe(data => {
      data.forEach(element => {
        this.clients.push(element);
      });

      this.clients.forEach((element, i) => {
        this.elements.push({
          clientid: element.ClientID,
          socketid: element.SocketID,
          ipaddress: element.ClientIP
        });

        this.mdbTable.setDataSource(this.elements);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
      });
    });
  }

  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(5);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }
}
