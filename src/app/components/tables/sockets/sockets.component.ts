import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { SocketioService } from 'src/app/services/socketio.service';
import { MdbTablePaginationComponent, MdbTableDirective } from 'angular-bootstrap-md';

@Component({
  selector: 'app-sockets',
  templateUrl: './sockets.component.html',
  styleUrls: ['./sockets.component.scss']
})
export class SocketsComponent implements OnInit {
  constructor(
    private cdRef: ChangeDetectorRef,
    private SocketService: SocketioService
  ) {}
  @ViewChild(MdbTablePaginationComponent, { static: true })
  mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;

  clients: any = [];
  elements: any = [];
  previous: any = [];

  headElements = ["SocketID", "ConnectionIP", "ACTIONS"];


  ngOnInit() {
    this.loadTableComponents();
  }

  loadTableComponents() {
    this.SocketService.getSocketClients().subscribe(data => {
      console.log(data);
      
      data.forEach(element => {
        this.clients.push(element);
      });

      this.clients.forEach((element, i) => {
        this.elements.push({
          socketid: element.SocketID,
          ipaddress: element.IP
        });

        this.mdbTable.setDataSource(this.elements);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();
      });
    });
  }

  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(15);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }
}
