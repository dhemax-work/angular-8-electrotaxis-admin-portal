import { Injectable } from "@angular/core";
import * as io from "socket.io-client";
import { environment } from "../../environments/environment";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class SocketioService {
  socket;

  constructor() {
    this.setupSocketConnection();
  }

  imConnected(){
    return Observable.create(observer => {
      observer.next(this.socket.connected);
    });
  }

  sendEvent(event, value = null) {
    if (value != null) {
      this.socket.emit(event, value);
    } else {
      this.socket.emit(event);
    }
  }

  setupSocketConnection() {
    this.socket = io(environment.SOCKET_ENDPOINT);
    this.GetAppData();
  }

  GetAppData() {
    setInterval(() => {
      this.getClientData();
      this.getVehiclesData();
      this.getSocketData();
    }, 1000);
  }

  getClientData() {
    this.socket.emit("getClients");
  }

  getVehiclesData() {
    this.socket.emit("getVehicles");
  }

  getSocketData(){
    this.socket.emit('getSocketClients');
  }

  getClients() {
    return Observable.create(observer => {
      this.socket.once("getClients", data => {
        observer.next(data);
      });
    });
  }

  getVehicles() {
    return Observable.create(observer => {
      this.socket.once("getVehicles", data => {
        observer.next(data);
      });
    });
  }


  getSocketClients(){
    return Observable.create(observer => {
      this.socket.once('getSocketClients', data => {
        observer.next(data);
      });
    });
  }
}
