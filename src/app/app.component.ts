import { Component } from '@angular/core';
import { SocketioService } from './services/socketio.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'taxiadmin';

  constructor(private socketService: SocketioService) {
    this.socketService.setupSocketConnection();
  }

}
