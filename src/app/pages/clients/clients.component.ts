import { NgModule } from "@angular/core";
import { Component, OnInit } from "@angular/core";
import { ClientsTableComponent } from "../../components/tables/clients-table/clients-table.component";
import { SocketioService } from "../../services/socketio.service";

@NgModule({
  declarations: [ClientsTableComponent]
})
@Component({
  selector: "app-clients",
  templateUrl: "./clients.component.html",
  styleUrls: ["./clients.component.scss"]
})
export class ClientsComponent implements OnInit {
  SocketClients: any = [];
  ClientsList: any = [];
  VehicleList: any = [];

  SocketsCount = 0;
  ClientsCount = 0;
  DriversCount = 0;

  isLoadedData = false;
  isSocketConnected = false;

  constructor(private SocketService: SocketioService) {}

  ngOnInit(): void {
    this.LoadClients();
  }

  LoadClients() {
    try {
      setInterval(() => {

        this.SocketService.imConnected().subscribe(data => {
          this.isSocketConnected = data;
        });


        if(this.isSocketConnected){
          this.SocketService.getClients().subscribe(data => {
            this.ClientsList = data;
            this.ClientsCount = data.length;
          });
          this.SocketService.getVehicles().subscribe(data => {
            this.VehicleList = data;
            this.DriversCount = data.length;
          });
          this.SocketService.getSocketClients().subscribe(data => {
            this.SocketsCount = data.length;
            this.SocketClients = data;
          });

          setTimeout(() => {
            this.isLoadedData = true;
          }, 3500);
        }else{
          this.isLoadedData = false;
        }

        
       
      }, 1500);
      
    } catch (error) {
      console.log("Timeout");
    }
  }
}
