import { NgModule } from "@angular/core";

import { Routes, RouterModule } from "@angular/router";
import { ClientsComponent } from "./pages/clients/clients.component";
import { HomeComponent } from "./pages/home/home.component";
import { MatchesComponent } from "./pages/matches/matches.component";

const routes: Routes = [
  {
    path: "home",
    component: HomeComponent
  },
  {
    path: "conns",
    component: ClientsComponent
  },
  {
    path: "matches",
    component: MatchesComponent
  },
  {
    path: "**",
    redirectTo: "home"
  }
];

@NgModule({
  declarations: [],
  imports: [
    //CommonModule
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
